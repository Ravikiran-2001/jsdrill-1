function lastCarInInventory(inventory) {
  // creating a function to find out last car in the inventory
  if (Array.isArray(inventory)) {
    // checking whether inventory is array or not
    let sizeInventory = inventory.length; // finding length of inventory array
    return inventory[sizeInventory - 1]; // returning last car details of array
  } else {
    return "Invalid array"; // if condition does'nt satisfy it will return
  }
}
module.exports = lastCarInInventory; // exporting function
