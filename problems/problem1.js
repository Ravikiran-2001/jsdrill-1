function findCarById(inventory, carId) {
  //creating function called findCarById and passed 2 arguments 1.Inventory array 2.given car id

  if (Array.isArray(inventory)) {
    // checking whether given variable inventory is array or not 
    for (let index = 0; index < inventory.length; index++) {
      if ("id" in inventory[index]) { // checking key id presence in given index of array
        if (inventory[index].id == carId) {
          //Iterating inside array and search for the car which matches carId which is provided
          return inventory[index];
        }
      }
    }
  } else {
    return "Invalid array or can't find key id"; // if above condition does'nt satisfy than following string will be returned
  }
}
module.exports = findCarById; //exporting our function
