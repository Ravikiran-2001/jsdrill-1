function allCarYears(inventory) {
  // creating function to fetch all car year
  let carYears = []; // creating empty array
  if (Array.isArray(inventory)) {
    // checking whether inventory is array or not
    for (let idx = 0; idx < inventory.length; idx++) {
      carYears.push(inventory[idx].car_year); // pushing car year in empty array
    }
    return carYears;
  } else {
    console.log("Innvalid array");
  }
}
module.exports = allCarYears; //exporting function
