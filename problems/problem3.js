function sortCarModelsAlphabetically(inventory) {
  // creating a function to sort all cars alphabetically
  let sortedCarModels = []; // creating an empty array
  if (Array.isArray(inventory)) {
    // checking whether inventory is array or not
    for (let idx = 0; idx < inventory.length; idx++) {
      sortedCarModels.push(inventory[idx].car_model); // pushing car_model data of all cars in our empty array
    }
    sortedCarModels.sort(); // sorting it by using sort method
    return sortedCarModels;
  } else {
    console.log("Invalid array");
  }
}
module.exports = sortCarModelsAlphabetically; // exporting function
