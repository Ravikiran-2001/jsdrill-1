function carsBeforeGivenYear(inventory, carDates) {
  // creating fuction to get cars details before year 2000
  let carDetails = []; // creating empty array
  if (Array.isArray(inventory)) {
    // checking whether inventory is array or not
    for (let idx = 0; idx < carDates.length; idx++) {
      if (carDates[idx] < 2000) {
        // checking whether car year is before year 2000
        carDetails.push(inventory[idx]); // if condition satisfies we will add car details in our new array
      }
    }
    return carDetails;
  } else {
    console.log("Invalid array");
  }
}
module.exports = carsBeforeGivenYear; // exporting function
