function findBmwAudiCars(inventory) {
  // creating function to filter out only BMW and Audi cars
  let carDetails = []; // creating empty array
  if (Array.isArray(inventory)) {
    // checking whether inventory is array or not
    for (let idx = 0; idx < inventory.length; idx++) {
      let model = inventory[idx].car_make;
      if (model === "Audi" || model === "BMW") {
        // checking whether car model is BMW or Audi
        carDetails.push(inventory[idx]); // if condition satisfies details will be pushed to new array
      }
    }
    return carDetails;
  } else {
    console.log("Invalid array");
  }
}
module.exports = findBmwAudiCars; // exporting function
