let inventory = require("../inventory"); // importing inventory array
let lastCarInInventory = require("../problems/problem2"); //importing function
let carDetails = lastCarInInventory(inventory); // calling the function
if (typeof carDetails === "string") {
  // checking our returned value is string or not
  console.log(carDetails);
} else {
  console.log("last car is a", carDetails.car_make, carDetails.car_model); // printing the output
}
