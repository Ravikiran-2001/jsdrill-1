let inventory = require("../inventory"); // importing inventory array
let sortCarModelsAlphabetically = require("../problems/problem3"); // importing function
const sortedCarModels = sortCarModelsAlphabetically(inventory); // calling the function
if (typeof sortedCarModels === "string") {
  console.log(sortedCarModels);
} else {
  console.log(sortedCarModels); // printing the output
}
