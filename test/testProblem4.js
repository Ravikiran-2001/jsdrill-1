let inventory = require("../inventory"); // importing inventory array
let allCarYears = require("../problems/problem4"); //importing function
let carDates = allCarYears(inventory); // calling function
if (typeof carDates === "string") {
  console.log(carDates);
} else {
  console.log(carDates); // printing output
}
module.exports = carDates; // exporting new array for next problem
