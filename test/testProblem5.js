let carDates = require("../test/testProblem4"); // importing carDates array
let inventory = require("../inventory"); // importing inventory array
let carsBeforeGivenYear = require("../problems/problem5"); // importing function
let resultArray = carsBeforeGivenYear(inventory, carDates); // calling function
if (typeof resultArray === "string") {
  console.log(resultArray);
} else {
  console.log(resultArray); //printing output
  console.log(resultArray.length); // printing length of returned array
}
