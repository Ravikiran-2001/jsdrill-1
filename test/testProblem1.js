let inventory = require("../inventory"); // importing inventory array
let findCarById = require("../problems/problem1"); // importing function
const carId = 33; // given car id
const carDetails = findCarById(inventory, carId); // calling the function
if (typeof carDetails === "string") {
  console.log(carDetails);
} else {
  console.log(
    "Car",
    carId,
    "is a",
    carDetails.car_year,
    carDetails.car_make,
    carDetails.car_model
  ); // printing the output
}
