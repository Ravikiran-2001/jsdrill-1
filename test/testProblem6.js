let inventory = require("../inventory"); // importing inventory array
let findBmwAudiCars = require("../problems/problem6"); // importing function
let result = findBmwAudiCars(inventory); // calling function
if (typeof result === "string") {
  console.log(result);
} else {
  const jasonString = JSON.stringify(result); // converting from object type to JSON type
  console.log(jasonString); // printing ouput
}
